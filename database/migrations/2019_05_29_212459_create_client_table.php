<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client', function (Blueprint $table) {
            $table->bigIncrements('id_client');
            $table->bigInteger('CI')->nullable();//cedula de identidad
            $table->string('name')->nullable();//nombre
			$table->string('lastname')->nullable();//apellido
            $table->string('address')->nullable();//dirrecion
			$table->string('city')->nullable();//ciudad
            $table->string('mail')->nullable();//correo
            $table->bigInteger('phone')->nullable();//telefono
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client');
    }
}
