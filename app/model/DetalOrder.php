<?php

namespace app\model;

use Illuminate\Database\Eloquent\Model;

class DetalOrder extends Model
{
    protected $table='detalorder';
    protected $primaryKey="id_detalorder";
    public $timestamps=true;
    const created_at = 'create_ad';
    const update_at = 'update_ad';
    protected $fillable =[
        'price_per_car',
        'quantity',
        'order_date',
        'return_date',
        'days',
		'namber_orders',
        'created_ad',
        'updated_ad'
    ];
}
