<?php

namespace app\model;

use Illuminate\Database\Eloquent\Model;

class Models extends Model
{
    protected $table='model';
    protected $primaryKey="id_model";
    public $timestamps=true;
    const created_at = 'create_ad';
    const update_at = 'update_ad';
    protected $fillable =[
        'name_model',
        'namber_of_seats',
        'manufacturing_date',
        'created_ad',
        'updated_ad'
    ];
}
