@extends('layouts.app')

@section('content')
<div class="container">
        <div class="container">
                @yield('main')
              </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"></div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>

                  <a class="nav-link" href="http://127.0.0.1:8000/DetalOrder"><i class="icon-basket-loaded"></i> Ver todos los alquileres</a>
                  <a class="nav-link" href="http://127.0.0.1:8000/Cost"><i class="icon-basket-loaded"></i>Todos los Cost</a>
                  <a class="nav-link" href="http://127.0.0.1:8000/Models"><i class="icon-basket-loaded"></i>Ver todos los models</a>
                  <a class="nav-link" href="http://127.0.0.1:8000/Order"><i class="icon-basket-loaded"></i>Ver las Ordenes</a>
                  <a class="nav-link" href="http://127.0.0.1:8000/Users"><i class="icon-basket-loaded"></i>Ver todos los usuarios</a>
                  <a class="nav-link" href="http://127.0.0.1:8000/Vehicle"><i class="icon-basket-loaded"></i>Descripcion de Autos</a>

            </div>
        </div>
    </div>
</div>
@endsection
