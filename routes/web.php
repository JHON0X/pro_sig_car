<?php
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


Route::get('/', function () {
    return view('welcome');
    });


Route::resource('/client','clientcontroller');
Route::post('/client','clientcontroller@store');
Route::resource('/Cost','CostController');
Route::resource('/DetalOrder','DetalOrderController');
Route::resource('/Models','ModelController');
Route::resource('/Acount','AcountController');
Route::resource('/Order','OrderController');
Route::resource('/Users','UsersController');
Route::resource('/Vehicle','VehicleController');

Auth::routes();
Route::get('/home','homeController@index')->name('home');
